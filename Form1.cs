﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PcapDotNet.Core;
using PcapDotNet.Analysis;
using PcapDotNet.Packets;
using PcapDotNet.Base;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.Transport;
using PcapDotNet.Packets.Http;
using PcapDotNet.Packets.Icmp;
using System.Net;
using System.Collections;
using System.IO;

namespace Proje_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
        }

       //Deneme deneme deneme request

        IDictionary<string, string> ipmac = new Dictionary<string, string>();
        List<string> iplist = new List<string>();
        List<string> maclist = new List<string>();


        private IList<LivePacketDevice> adaptör_listesi;
        private PacketDevice seçili_adaptör;
        ArrayList hayrıntı = new ArrayList();
        ArrayList kayrıntı = new ArrayList();
        ArrayList dayrıntı = new ArrayList();


        string kaynakip=""; 
        string hedefip="";
        string protokol="";
        string zamanı="";
        string http_headırı="";
        string paketboyutu="";
        string ipuzunlugu="";
        string tcpkaynakport=""; 
        string tcphedefport="";
        string tcp_alındınumarası="";
        string tcpsıra_numarası="";
        string tcp_sıradaknumara = ""; string tcpwindow = "";
        string udpkaynak="";string udphedef="";
        string icmpkod = ""; string ıcmpmesajtipi = "";
        string icmpuzunluk = ""; string icmppayload = "";
        string ipm = ""; string mac = "";
        int i = -1;
        
        private void Form1_Load(object sender, EventArgs e)
        {
            ipm = IPogren();          
            button_Durdur.Enabled = false;           
            adaptör_listesi = LivePacketDevice.AllLocalMachine;

            listView1.View = View.Details;
            listView1.Columns.Add("Zamanı", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("Kaynak İp",100,HorizontalAlignment.Left);
            listView1.Columns.Add("Hedef İp", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("Protokol", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("Boyut", 100, HorizontalAlignment.Left);


            listView2.View = View.Details;
            listView2.Columns.Add("Zamanı", 100, HorizontalAlignment.Left);
            listView2.Columns.Add("Kaynak İp", 100, HorizontalAlignment.Left);
            listView2.Columns.Add("Hedef İp", 100, HorizontalAlignment.Left);
            listView2.Columns.Add("Protokol", 100, HorizontalAlignment.Left);
            listView2.Columns.Add("Boyut", 100, HorizontalAlignment.Left);


            listView3.View = View.Details;
            listView3.Columns.Add("Zamanı", 100, HorizontalAlignment.Left);
            listView3.Columns.Add("Kaynak İp", 100, HorizontalAlignment.Left);
            listView3.Columns.Add("Hedef İp", 100, HorizontalAlignment.Left);
            listView3.Columns.Add("Protokol", 100, HorizontalAlignment.Left);
            listView3.Columns.Add("Boyut", 100, HorizontalAlignment.Left);

            
            PcapDotNetAnalysis.OptIn = true;

            if (adaptör_listesi.Count == 0)
            {
                MessageBox.Show("Listbox Boş !!!!");
                return;
           }

            for (int i = 0; i != adaptör_listesi.Count; ++i)
            {
                LivePacketDevice Adapter = adaptör_listesi[i];

                if (Adapter.Description != null)

                    listBox1_Adaptör.Items.Add(Adapter.Description);
                else
                    listBox1_Adaptör.Items.Add("Unknown");
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            using (PacketCommunicator bagdaştırıcı = seçili_adaptör.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 1000))
            {

                if (checkBox_TCP.Checked && !checkBox_UDP.Checked)
                {
                    using (BerkeleyPacketFilter filter = bagdaştırıcı.CreateFilter("tcp"))
                    {
                        bagdaştırıcı.SetFilter(filter);
                    }
                }
                else if (checkBox_UDP.Checked && !checkBox_TCP.Checked)
                {
                    using (BerkeleyPacketFilter filter = bagdaştırıcı.CreateFilter("udp"))
                    {
                        bagdaştırıcı.SetFilter(filter);
                    }
                }
                else if (checkBox_TCP.Checked && checkBox_UDP.Checked)
                {
                    using (BerkeleyPacketFilter filter = bagdaştırıcı.CreateFilter("ip and udp"))
                    {
                        bagdaştırıcı.SetFilter(filter);
                    }
                }

                bagdaştırıcı.ReceivePackets(0, PaketYakala);
            }
        }
       
        private void PaketYakala(Packet paket)
        {
            this.udphedef = ""; this.udpkaynak = ""; this.protokol = ""; this.kaynakip = "";
            this.hedefip = ""; this.zamanı = ""; this.paketboyutu = ""; this.tcp_alındınumarası = "";
            this.tcp_sıradaknumara = ""; this.tcphedefport = ""; this.tcpkaynakport = ""; this.http_headırı = "";
            this.ipuzunlugu = ""; this.paketboyutu = ""; this.mac = "";
            

          IpV4Datagram  ip = paket.Ethernet.IpV4;
          TcpDatagram tcp = ip.Tcp;
          UdpDatagram udp = ip.Udp;
          HttpDatagram http = null;
                          
          
            if (ip.Protocol.ToString().Equals("Tcp"))
            {
                http = tcp.Http;

                if (http.Header != null && !checkBox_TCP.Checked)
                {
                   
                    protokol = "HTTP";
                    paketboyutu = paket.Count.ToString();
                    zamanı = paket.Timestamp.ToString();
                    kaynakip = ip.Source.ToString();
                    hedefip = ip.Destination.ToString();
                    http_headırı = http.Header.ToString();
                    ipuzunlugu = ip.Length.ToString();
                    mac = paket.Ethernet.Source.ToString();


                    if (!ipmac.ContainsKey(mac))
                    {
                        ipmac.Add(mac, kaynakip);
                    }
                    else
                    {
                        if (kaynakip != ipmac[mac])
                        {
                            listBox_ipmac.Items.Add(kaynakip+"  "+ipmac[mac]);
                        }
                    }

                    if (!iplist.Contains(kaynakip))
                    {
                        iplist.Add(kaynakip);
                        maclist.Add(mac);
                        i++;
                    }
                   
                   
                
                    icmpkod = ip.Icmp.Code.ToString();
                    ıcmpmesajtipi = ip.Icmp.MessageType.ToString();
                    icmpuzunluk = ip.Icmp.Length.ToString();
                    icmppayload = ip.Icmp.Payload.ToString();
                }
                else
                {
                   
                    protokol = ip.Protocol.ToString();
                    paketboyutu = paket.Count.ToString();
                    zamanı = paket.Timestamp.ToString();
                    kaynakip = ip.Source.ToString();
                    hedefip = ip.Destination.ToString();
                    ipuzunlugu = ip.Length.ToString();
                    tcp_alındınumarası = tcp.AcknowledgmentNumber.ToString();
                    tcphedefport = tcp.DestinationPort.ToString();
                    tcpkaynakport = tcp.SourcePort.ToString();
                    tcp_sıradaknumara = tcp.NextSequenceNumber.ToString();
                    tcpsıra_numarası = tcp.SequenceNumber.ToString();
                    tcpwindow = tcp.Window.ToString();
                    mac = paket.Ethernet.Source.ToString();

                    if (!iplist.Contains(kaynakip))
                    {
                        iplist.Add(kaynakip);
                        maclist.Add(mac);
                        i++;
                    }


                    if (!ipmac.ContainsKey(mac))
                    {
                        ipmac.Add(mac, kaynakip);
                    }
                    else
                    {
                        if (kaynakip != ipmac[mac])
                        {
                            listBox_ipmac.Items.Add(kaynakip + "  " + ipmac[mac]);
                        }
                    }

                    icmpkod = ip.Icmp.Code.ToString();
                    ıcmpmesajtipi = ip.Icmp.MessageType.ToString();
                    icmpuzunluk = ip.Icmp.Length.ToString();
                    icmppayload = ip.Icmp.Payload.ToString();

                }

            }
            else
            {

                if (ip.Protocol.ToString().Equals("Udp"))
                {
                    protokol = ip.Protocol.ToString();
                    kaynakip = ip.Source.ToString();
                    hedefip = ip.Destination.ToString();
                    zamanı = paket.Timestamp.ToString();
                    paketboyutu = paket.Count.ToString();
                    ipuzunlugu = ip.Length.ToString();
                    udpkaynak = udp.SourcePort.ToString();
                    udphedef = udp.DestinationPort.ToString();
                    mac = paket.Ethernet.Source.ToString();

                    if (!iplist.Contains(kaynakip))
                    {
                        iplist.Add(kaynakip);
                        maclist.Add(mac);
                        i++;
                    }

                    if (!ipmac.ContainsKey(mac))
                    {
                        ipmac.Add(mac, kaynakip);
                    }
                    else
                    {
                        if (kaynakip != ipmac[mac])
                        {
                            listBox_ipmac.Items.Add(kaynakip + "  " + ipmac[mac]);
                        }
                    }
                                

                    icmpkod = ip.Icmp.Code.ToString();
                    ıcmpmesajtipi = ip.Icmp.MessageType.ToString();
                    icmpuzunluk = ip.Icmp.Length.ToString();
                    icmppayload = ip.Icmp.Payload.ToString();
                }
                else
                {
                    protokol = ip.Protocol.ToString();
                    hedefip = ip.Destination.ToString();
                    kaynakip = ip.Source.ToString();
                    zamanı = paket.Timestamp.ToString();
                    paketboyutu = paket.Count.ToString();
                    ipuzunlugu = ip.Length.ToString();
                    mac = paket.Ethernet.Source.ToString();

                    if (!iplist.Contains(kaynakip))
                    {
                        iplist.Add(kaynakip);
                        maclist.Add(mac);
                        i++;
                    }

                    if (!ipmac.ContainsKey(mac))
                    {
                        ipmac.Add(mac, kaynakip);
                    }
                    else
                    {
                        if (kaynakip != ipmac[mac])
                        {
                            listBox_ipmac.Items.Add(kaynakip + "  " + ipmac[mac]);
                        }
                    }

                    icmpkod = ip.Icmp.Code.ToString();
                    ıcmpmesajtipi = ip.Icmp.MessageType.ToString();
                    icmpuzunluk = ip.Icmp.Length.ToString();
                    icmppayload = ip.Icmp.Payload.ToString();
                }
            }
        }
        
        private void button_baslat_Click(object sender, EventArgs e)
        {

            button_baslat.Enabled = false;
            button_Durdur.Enabled = true;
            listBox1_Adaptör.Enabled = false;
            checkBox_TCP.Enabled = false;
            checkBox_UDP.Enabled = false;

            if (listBox1_Adaptör.SelectedIndex >= 0)
            {
                timer1.Enabled = true;
                seçili_adaptör = adaptör_listesi[listBox1_Adaptör.SelectedIndex];
                backgroundWorker1.RunWorkerAsync();
                listBox1_Adaptör.Enabled = false;
            }
            else 
            {
                MessageBox.Show("LİSTBOX'DAN ADAPTÖR SEÇİNİZ!!");
              
            }
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            ListViewItem elemanlar = new ListViewItem(zamanı);
            elemanlar.SubItems.Add(kaynakip);
            elemanlar.SubItems.Add(hedefip);
            elemanlar.SubItems.Add(protokol);
            elemanlar.SubItems.Add(ipuzunlugu);

            ListViewItem elemanlar1 = new ListViewItem(zamanı);
            elemanlar1.SubItems.Add(kaynakip);
            elemanlar1.SubItems.Add(hedefip);
            elemanlar1.SubItems.Add(protokol);
            elemanlar1.SubItems.Add(ipuzunlugu);
            
            ListViewItem elemanlar2 = new ListViewItem(zamanı);
            elemanlar2.SubItems.Add(kaynakip);
            elemanlar2.SubItems.Add(hedefip);
            elemanlar2.SubItems.Add(protokol);
            elemanlar2.SubItems.Add(ipuzunlugu);
            string ekle="";
          
            if (kaynakip == ipm) 
            {
                listView1.Items.Insert(0, elemanlar);  
            }
            else
            {
                if (hedefip == ipm) 
                {
                    listView2.Items.Insert(0, elemanlar1);  
                }
                else 
                {
                    listView3.Items.Insert(0, elemanlar2);  
                }
            }
            
            
            if (!paketboyutu.Equals("")) 
            {
                if (protokol.Equals("Tcp"))
                {

                    textBox1.Text = "Protokolu :" + protokol + System.Environment.NewLine +"TCP kaynak portu :"+tcpkaynakport+System.Environment.NewLine+"Tcp hedefe portu :" + tcphedefport + System.Environment.NewLine +"TCP Alındı numarası :"+ tcp_alındınumarası + System.Environment.NewLine +"Tcp Sıra numarası :"+ tcpsıra_numarası + System.Environment.NewLine +"Tcp Sıradakı Numara :"+tcp_sıradaknumara + System.Environment.NewLine+"TCP Window : "+tcpwindow+System.Environment.NewLine+
                        "----------------İCMP-----------------------" + System.Environment.NewLine + "İCMP KODU :" + icmpkod + System.Environment.NewLine +
                        "İCMP MESAJ Tipi :"+ıcmpmesajtipi+System.Environment.NewLine+"İCMP uzunlugu :"+icmpuzunluk +System.Environment.NewLine+"İcmp Payload : "+icmppayload;
                    kayrıntı.Add(textBox1.Text);
                 
                    //ekle = iplist[i] + "---" + maclist[i];
                    //listBox1.Items.Add(ekle);
                 


                }
                else
                {
                    if (protokol.Equals("Http"))
                    {
                        textBox1.Text =  "Protokolu :" + protokol + System.Environment.NewLine +"HTTP Headers :"+ http_headırı+System.Environment.NewLine;
                        //ekle = iplist[i] + "---" + maclist[i];
                        //listBox1.Items.Add(ekle);
                  
                    }
                    else
                    {
                        if (protokol.Equals("Udp"))
                        {                        
                            textBox1.Text = "Protokolu :" + protokol + System.Environment.NewLine + "Udp hedef Port : " + udphedef + System.Environment.NewLine + "UDP Kaynak Port : " + udpkaynak+System.Environment.NewLine+
                            "---------İCMP-----------" + System.Environment.NewLine + "İCMP KODU :" + icmpkod + System.Environment.NewLine + "İCMP MESAJ TİPİ :" + ıcmpmesajtipi + System.Environment.NewLine + "İCMP uzunlugu :" + icmpuzunluk + System.Environment.NewLine + "İcmp Payload : " + icmppayload;

                            //ekle = iplist[i] + "---" + maclist[i];
                            //listBox1.Items.Add(ekle);
                           
                        }
                    }
                }
            


            }
        }  
        
        private void button_Durdur_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            button_Durdur.Enabled = false;
            button_baslat.Enabled = true;
            listBox1_Adaptör.Enabled = true;
            checkBox_TCP.Enabled = true;
            checkBox_UDP.Enabled = true;
        }

        public string IPogren()
        {

            string HostAdi = System.Net.Dns.GetHostName();

            IPHostEntry ipGiris = System.Net.Dns.GetHostEntry(HostAdi);

            IPAddress[] ipAdresleri = ipGiris.AddressList;

            return ipAdresleri[ipAdresleri.Length - 2].ToString();

        }
    }
}
